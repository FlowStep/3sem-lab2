//
// Created by yuri on 12.03.17.
//

#ifndef INC_3SEM_LAB2_MATRIXSLICE_HPP
#define INC_3SEM_LAB2_MATRIXSLICE_HPP


#include <cstdlib>
#include "Matrix.hpp"

class Matrix;
class Vector;

class MatrixSlice {
private:
    const Matrix *parent;
    size_t rows, cols;
    size_t rbegin, rend, rstep;
    size_t cbegin, cend, cstep;
public:
    MatrixSlice(const Matrix *parent_, size_t rbegin_, size_t rend_,
                size_t rstep_, size_t cbegin_, size_t cend_, size_t cstep_);
    MatrixSlice(const Matrix *parent_, size_t rbegin_, size_t rend_, size_t cbegin_, size_t cend_);

    double operator()(size_t row, size_t col) const;

    Matrix transposed();
    size_t rowsCount() const;
    size_t colsCount() const;
    Vector getCol(size_t col) const;
    Vector getRow(size_t row) const;

    Matrix operator-() const;

    Matrix operator-(MatrixSlice const &m) const;
    Matrix operator+(MatrixSlice const &m) const;
    Matrix operator*(double d) const;
    Matrix operator/(double d) const;
    Matrix operator*(MatrixSlice const &m) const;
    //Vector operator*(Vector const &v) const;

    MatrixSlice(MatrixSlice const &&ms);

    MatrixSlice(MatrixSlice const&) = delete;
    MatrixSlice &operator=(MatrixSlice const&) = delete;
};


#endif //INC_3SEM_LAB2_MATRIXSLICE_HPP
