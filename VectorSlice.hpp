//
// Created by stephen on 3/12/17.
//

#ifndef INC_3SEM_LAB2_VECTORSLICE_HPP
#define INC_3SEM_LAB2_VECTORSLICE_HPP


#include <cstdlib>
#include "Vector.hpp"

class Vector;

class VectorSlice {
private:
    const Vector *parent;
    size_t size;
    size_t begin, end, step;

public:
    VectorSlice(Vector const *parent_, size_t begin_, size_t end_, size_t step_ = 1);

    double operator[](size_t i) const;

    Vector operator-() const;
    Vector operator-(VectorSlice const &v) const;
    Vector operator+(VectorSlice const &v) const;
    Vector operator*(double d) const;
    Vector operator/(double d) const;

    double scalar(VectorSlice const &v) const;
    Vector transposed();
    size_t getSize() const;

    VectorSlice(VectorSlice const&&vs);

    VectorSlice(VectorSlice const&) = delete;
    VectorSlice &operator=(VectorSlice const&) = delete;

    friend class Vector;
};

//Vector operator-(Vector const &v, VectorSlice &vs);
//Vector operator*(double d, VectorSlice &vs);


#endif //INC_3SEM_LAB2_VECTORSLICE_HPP
