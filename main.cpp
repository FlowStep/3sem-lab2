#include <iostream>
#include "Vector.hpp"
#include "Matrix.hpp"

int main() {
    Vector v1(10);

    for (int i = 0; i < v1.getSize(); ++i)
        v1[i] = i + 1;

    Vector v2(v1);

    v2 += v1;

    for (int i = 0; i < v1.getSize(); ++i)
        std::cout << v1[i] << ' ';
    std::cout << std::endl;

    for (int i = 0; i < v2.getSize(); ++i)
        std::cout << v2[i] << ' ';
    std::cout << std::endl;

    Matrix m1(3, 3);
    Matrix m2(m1);

    m2(1, 1) = 5;

    for (int i = 0; i < m1.rowsCount(); ++i) {
        for (int j = 0; j < m1.colsCount(); ++j)
            std::cout << m1(i, j) << '\t';
        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (int i = 0; i < m2.rowsCount(); ++i) {
        for (int j = 0; j < m2.colsCount(); ++j)
            std::cout << m2(i, j) << '\t';
        std::cout << std::endl;
    }

    std::cout << std::endl;

    Matrix m3(9, 5);

    for (int i = 0; i < m3.rowsCount(); ++i)
        for (int j = 0; j < m3.colsCount(); ++j)
            m3(i, j) = i * m3.colsCount() + j;

    m3 *= 5;

    for (int i = 0; i < m3.rowsCount(); ++i) {
        for (int j = 0; j < m3.colsCount(); ++j)
            std::cout << m3(i, j) << '\t';
        std::cout << std::endl;
    }

    std::cout << std::endl;
    m3.transpose();

    std::cout << m3 << std::endl;

    Matrix m4 = m3.slice(0, m3.rowsCount() - 1, 2, 0, m3.colsCount() - 1, 2) / 10;

    std::cout << m4 << std::endl;

    Vector v4 = m4.getCol(2);

    std::cout << v4 << std::endl;

    Vector v5 = m4.getRow(2);

    Vector v6 = v5;

    double d = v6[0] + v6[1] * v6[2];

    std::cout << d << std::endl;

    std::cout << v5 << std::endl;
    std::cout << v6 << std::endl;

    Vector v7 = v5.slice(1, 3) + v6.slice(1, 3);

    Vector v8 = v7 + v7 + v5.slice(1, 3) + v7;

    std::cout << v8 << std::endl;

    {
        std::cout  << "qq " << std::endl;
        Matrix m1(3, 3), m2(3, 3);
        for (size_t i = 0; i < m1.rowsCount(); i++)
            for (size_t j = 0; j < m1.colsCount(); j++) {
                m1(i, j) = i + j * 2;
                m2(i, j) = j + i * 3;
            }

        Matrix m3 = m1.slice(0, 2, 2, 0, 2, 2);
        Matrix m4 = m3 + m2.slice(0, 2, 2, 0, 2, 2) - m3;
        std::cout << "m1\n" << m1 << std::endl;
        std::cout << "m2\n" << m2 << std::endl;
        std::cout << "m3\n" << m3 << std::endl;
        std::cout << "m4\n" << m4 << std::endl;
    }

    return 0;
}