//
// Created by stephen on 3/11/17.
//

#ifndef INC_3SEM_LAB2_ELEMPROXY_HPP
#define INC_3SEM_LAB2_ELEMPROXY_HPP

template <typename T>
class ElemProxy {
private:
    T *parent;
    size_t pos;
public:
    ElemProxy(T *parent_, size_t pos_) : parent(parent_), pos(pos_) {}

    ElemProxy<T> &operator=(double d) const {
        parent->setElem(pos, d);
    }

    operator double() const {
        return parent->getElem(pos);
    }
};

#endif //INC_3SEM_LAB2_ELEMPROXY_HPP
