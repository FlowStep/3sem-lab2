//
// Created by yuri on 05.03.17.
//

#include <algorithm>
#include "Matrix.hpp"

//Переезд с двумерного массива на одномерный был осуществлен одной регуляркой
//   %s/\(\[\)\([a-z]\+\)\]\[\([a-z]\+\)\(\]\)/\1\2 * cols + \3\4/g    (vim)

Matrix::Matrix(size_t rows_, size_t cols_) : rows(rows_), cols(cols_) {
    data = new double[rows * cols];

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            data[i * cols + j] = 0;
}

Matrix::Matrix(MatrixSlice const &ms) : rows(ms.rowsCount()), cols(ms.colsCount()) {
    data = new double[rows * cols];

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            data[i * cols + j] = ms(i, j);
}

Matrix Matrix::operator+(Matrix const &m) const {
    if (rows != m.rows || cols != m.cols)
        throw -1;

    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] = data[i * cols + j] + m.data[i * cols + j];

    return res;
}

Matrix Matrix::operator-(Matrix const &m) const {
    if (rows != m.rows || cols != m.cols)
        throw -1;

    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] = data[i * cols + j] - m.data[i * cols + j];

    return res;
}

Matrix Matrix::operator*(double d) const {
    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] = data[i * cols + j] * d;

    return res;
}

Matrix Matrix::transposed() {
    Matrix res(cols, rows);

    for (size_t i = 0; i < cols; i++)
        for (size_t j = 0; j < rows; j++)
            res.data[i * res.cols + j] = data[j * cols + i];

    return res;
}

void Matrix::transpose() {
    *this = this->transposed();
}

Matrix Matrix::operator*(Matrix const &m) const {
    if (cols != m.rows)
        throw -1;

    Matrix res(rows, m.cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < m.cols; j++)
            for (size_t k = 0; k < m.cols; k++)
                res.data[i * cols + j] += data[i * cols + k] * m.data[k * cols + j];

    return res;
}

Matrix &Matrix::operator=(Matrix const &m) {
    data = m.data;
    rows = m.rows;
    cols = m.cols;

    return *this;
}

Matrix &Matrix::operator=(MatrixSlice const &ms) {
    data = new double[rows * cols];
    rows = ms.rowsCount();
    cols = ms.colsCount();

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            data[i * cols + j] = ms(i, j);
}

void Matrix::copyOnWrite() {
    std::cout << "Copy : Matrix\n";

    SPtr<double> tmp = new double[rows * cols];

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            tmp[i * cols + j] = data[i * cols + j];

    data = tmp;
}

double Matrix::operator()(size_t row, size_t col) const {
    return data[row * cols + col];
}

ElemProxy<Matrix> Matrix::operator()(size_t row, size_t col) {
    return ElemProxy<Matrix>(this, row * cols + col);
}

Matrix &Matrix::operator-=(Matrix const &m) {
    if (cols != m.rows)
        throw -1;

    if (data.ref_count() > 1)
        copyOnWrite();

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            data[i * cols + j] -= m.data[i * cols + j];

    return *this;
}

Matrix &Matrix::operator+=(Matrix const &m) {
    if (cols != m.rows)
        throw -1;

    if (data.ref_count() > 1)
        copyOnWrite();

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            data[i * cols + j] += m.data[i * cols + j];

    return *this;
}

Matrix &Matrix::operator*=(double d) {
    if (data.ref_count() > 1)
        copyOnWrite();

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            data[i * cols + j] *= d;

    return *this;
}

Matrix Matrix::operator-() const {
    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] -= data[i * cols + j];

    return res;
}

Matrix Matrix::operator/(double d) const {
    Matrix res(rows, cols);

    for (size_t i = 0; i < rows; ++i)
        for (size_t j = 0; j < cols; ++j)
            res.data[i * res.cols + j] = data[i * cols + j] / d;

    return res;
}

Matrix &Matrix::operator/=(double d) {
    if (data.ref_count() > 1)
        copyOnWrite();

    for (size_t i = 0; i < rows; ++i)
        for (size_t j = 0; j < cols; ++j)
            data[i * cols + j] /= d;

    return *this;
}

size_t Matrix::rowsCount() const {
    return rows;
}

size_t Matrix::colsCount() const {
    return cols;
}

MatrixSlice Matrix::slice(size_t rbegin, size_t rend, size_t rstep, size_t cbegin, size_t cend, size_t cstep) const {
    if (rstep == 0 || cstep == 0)
        throw -1;

    MatrixSlice res(this, rbegin, rend, rstep, cbegin, cend, cstep);

    return res;
}

Matrix &Matrix::operator*=(Matrix const &m) {
    if (data.ref_count() > 1)
        copyOnWrite();

    *this = *this * m;

    return *this;
}

Vector Matrix::operator*(Vector const &v) const {
    if (cols != v.getSize() && v.getMetric() == Vector::orientation::col)
        throw -1;

    Vector res(v.getSize(), Vector::orientation::col);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < v.getSize(); j++)
            res.data[i] += data[i * cols + j] * v[j];

    return res;
}

Vector Matrix::getRow(size_t row) const {
    Vector res(cols);
    for (size_t i = 0; i < cols; i++)
        res[i] = data[row * cols + i];

    return res;
}

Vector Matrix::getCol(size_t col) const {
    Vector res(rows, Vector::orientation::col);
    for (size_t i = 0; i < rows; i++)
        res[i] = data[i * cols + col];

    return res;
}

void Matrix::setElem(size_t pos, double val) {
    if (data.ref_count() > 1)
        copyOnWrite();

    data[pos] = val;
}

double Matrix::getElem(size_t pos) const {
    return data[pos];
}

Matrix::operator MatrixSlice() const {
    return MatrixSlice(this,0, rows - 1, 1, 0, cols - 1, 1);
}

Matrix Matrix::operator+(MatrixSlice const &ms) {
    return ms + *this;
}

Matrix Matrix::operator-(MatrixSlice const &ms) {
    return (MatrixSlice)*this - ms;
}

Matrix Matrix::operator*(MatrixSlice const &ms) {
    return (MatrixSlice)*this * ms;
}

Matrix operator*(double d, Matrix const &m) {
    return m * d;
}

std::ostream &operator<<(std::ostream &os, const Matrix &m) {
    for (size_t i = 0; i < m.rowsCount(); i++) {
        for (size_t j = 0; j < m.colsCount(); j++)
            os << m(i, j) << ' ';
        os << std::endl;
    }

    return os;
}
