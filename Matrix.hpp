//
// Created by yuri on 05.03.17.
//

#ifndef INC_3SEM_LAB2_MATRIX_HPP
#define INC_3SEM_LAB2_MATRIX_HPP

#include "SPtr.hpp"
#include "Vector.hpp"
#include "ElemProxy.hpp"
#include "MatrixSlice.hpp"

class Vector;
class MatrixSlice;

class Matrix {
private:
    SPtr<double> data;
    size_t rows;
    size_t cols;

    void copyOnWrite();
    void setElem(size_t pos, double val);
    double getElem(size_t pos) const;
public:
    Matrix(size_t rows_, size_t cols_);
    Matrix(MatrixSlice const &ms);

    Matrix transposed();
    void transpose();
    size_t rowsCount() const;
    size_t colsCount() const;
    MatrixSlice slice(size_t rbegin, size_t rend, size_t rstep, size_t cbegin, size_t cend, size_t cstep) const;
    Vector getCol(size_t col) const;
    Vector getRow(size_t row) const;

    //double *operator[](size_t i) const;
    double operator()(size_t row, size_t col) const;
    ElemProxy<Matrix> operator()(size_t row, size_t col);
    Matrix operator-() const;

    Matrix &operator=(Matrix const &m);
    Matrix &operator=(MatrixSlice const &ms);
    operator MatrixSlice() const;

    Matrix operator-(Matrix const &m) const;
    Matrix operator+(Matrix const &m) const;
    Matrix operator*(double d) const;
    Matrix operator/(double d) const;
    Matrix operator*(Matrix const &m) const;
    Vector operator*(Vector const &v) const;

    Matrix &operator-=(Matrix const &m);
    Matrix &operator+=(Matrix const &m);
    Matrix &operator*=(Matrix const &m);
    Matrix &operator*=(double d);
    Matrix &operator/=(double d);

    Matrix operator-(MatrixSlice const &ms);
    Matrix operator+(MatrixSlice const &ms);
    Matrix operator*(MatrixSlice const &ms);

    friend class ElemProxy<Matrix>;
    friend class MatrixSlice;
    friend class Vector;
};

Matrix operator*(double d, Matrix const &m);
std::ostream &operator<<(std::ostream &os, const Matrix &m);

#endif //INC_3SEM_LAB2_MATRIX_HPP
