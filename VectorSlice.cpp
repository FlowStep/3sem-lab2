//
// Created by stephen on 3/12/17.
//

#include "VectorSlice.hpp"

VectorSlice::VectorSlice(Vector const *parent_, size_t begin_, size_t end_, size_t step_)
        : parent(parent_), begin(begin_), end(end_), step(step_) {
    size = (end - begin) / step + 1;
}

double VectorSlice::operator[](size_t i) const {
    return parent->getElem(begin + step * i);
}

Vector VectorSlice::operator-() const {
    Vector res(size);

    for (size_t i = 0; i < size; ++i)
        res[i] = -(*this)[i];

    return res;
}

Vector VectorSlice::operator-(VectorSlice const &v) const {
    Vector res(size);

    for (size_t i = 0; i < size; ++i)
        res[i] = (*this)[i] - v[i];

    return res;
}

Vector VectorSlice::operator+(VectorSlice const &v) const {
    Vector res(size);

    for (size_t i = 0; i < size; ++i)
        res[i] = (*this)[i] + v[i];

    return res;
}

Vector VectorSlice::operator*(double d) const {
    Vector res(size);

    for (size_t i = 0; i < size; ++i)
        res[i] = (*this)[i] * d;

    return res;
}

Vector VectorSlice::operator/(double d) const {
    Vector res(size);

    for (size_t i = 0; i < size; ++i)
        res[i] = (*this)[i] / d;

    return res;
}

double VectorSlice::scalar(VectorSlice const &v) const {
    double res = 0;

    for (size_t i = 0; i < size; ++i)
        res += (*this)[i] * v[i];

    return res;
}

Vector VectorSlice::transposed() {
}

size_t VectorSlice::getSize() const {
    return size;
}

VectorSlice::VectorSlice(VectorSlice const &&vs) {
    parent = vs.parent;
    size = vs.size;
    begin = vs.begin;
    end = vs.end;
    step = vs.step;
}

Vector operator-(Vector const &v, VectorSlice &vs) {
}

Vector operator+(Vector &v, VectorSlice &vs) {
    return vs + v;
}
