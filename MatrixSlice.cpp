//
// Created by yuri on 12.03.17.
//

#include "MatrixSlice.hpp"

MatrixSlice::MatrixSlice(const Matrix *parent_, size_t rbegin_, size_t rend_, size_t rstep_,
                         size_t cbegin_, size_t cend_, size_t cstep_)
        : parent(parent_), rbegin(rbegin_),
          rend(rend_), rstep(rstep_), cbegin(cbegin_), cend(cend_), cstep(cstep_) {
    rows = (rend - rbegin) / rstep + 1;
    cols = (cend - cbegin) / cstep + 1;
}

MatrixSlice::MatrixSlice(const Matrix *parent_, size_t rbegin_, size_t rend_, size_t cbegin_, size_t cend_)
        : parent(parent_), rbegin(rbegin_), rend(rend_), rstep(1),
          cbegin(cbegin_), cend(cend_), cstep(1) {
    rows = (rend - rbegin) + 1;
    cols = (cend - cbegin) + 1;
}

double MatrixSlice::operator()(size_t row, size_t col) const {
    return parent->getElem((rbegin + rstep * row) * parent->colsCount() + cbegin + cstep * col);
}

Matrix MatrixSlice::operator-() const {
    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; ++i)
        for (size_t j = 0; j < cols; ++j)
            res.data[i * res.cols + j] -= (*this)(i, j);

    return res;
}

Matrix MatrixSlice::operator-(MatrixSlice const &m) const {
    if (rows != m.rows || cols != m.cols)
        throw -1;

    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] = (*this)(i, j) - m(i, j);

    return res;
}

Matrix MatrixSlice::operator+(MatrixSlice const &m) const {
    if (rows != m.rows || cols != m.cols)
        throw -1;

    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] = (*this)(i, j) + m(i, j);

    return res;
}

Matrix MatrixSlice::operator*(double d) const {
    Matrix res(rows, cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            res.data[i * cols + j] = (*this)(i, j) * d;

    return res;
}

Matrix MatrixSlice::operator/(double d) const {
    Matrix res(rows, cols);

    for (size_t i = 0; i < rows; ++i)
        for (size_t j = 0; j < cols; ++j)
            res.data[i * res.cols + j] = (*this)(i, j) / d;

    return res;
}

Matrix MatrixSlice::operator*(MatrixSlice const &m) const {
    if (cols != m.rows)
        throw -1;

    Matrix res(rows, m.cols);
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < m.cols; j++)
            for (size_t k = 0; k < m.cols; k++)
                res.data[i * cols + j] += (*this)(i, k) * m(k, j);

    return res;
}

size_t MatrixSlice::rowsCount() const {
    return rows;
}

size_t MatrixSlice::colsCount() const {
    return cols;
}

Vector MatrixSlice::getCol(size_t col) const {
    Vector res(rows, Vector::orientation::col);
    for (size_t i = 0; i < rows; i++)
        res[i] = (*this)(i, col);

    return res;
}

Vector MatrixSlice::getRow(size_t row) const {
    Vector res(cols);
    for (size_t i = 0; i < cols; i++)
        res[i] = (*this)(row, i);

    return res;
}

Matrix MatrixSlice::transposed() {
    Matrix res(cols, rows);

    for (size_t i = 0; i < cols; i++)
        for (size_t j = 0; j < rows; j++)
            res.data[i * res.cols + j] = (*this)(j, i);

    return res;
}

MatrixSlice::MatrixSlice(MatrixSlice const &&ms) {
    parent = ms.parent;

    rows = ms.rows;
    cols = ms.cols;

    rbegin = ms.rbegin;
    rend = ms.rend;
    rstep = ms.rstep;

    cbegin = ms.cbegin;
    cend = ms.cend;
    cstep = ms.cstep;
}
