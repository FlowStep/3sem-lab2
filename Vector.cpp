//
// Created by stephen on 3/4/17.
//

#include "Vector.hpp"

Vector::Vector(size_t n, orientation m)
        : data(new double[n]), metric(m), size(n) {
    for (size_t i = 0; i < size; ++i)
        data[i] = 0;
}

double Vector::operator[](size_t i) const {
    return data[i];
}

ElemProxy<Vector> Vector::operator[](size_t i) {
    return ElemProxy<Vector>(this, i);
}

Vector Vector::operator*(double d) const {
    Vector res(size);
    for (size_t i = 0; i < size; ++i)
        res[i] = data[i] * d;

    return res;
}

Vector Vector::operator+(Vector const &v) const {
    if (size != v.size)
        throw -1;

    Vector res(size);
    for (size_t i = 0; i < size; ++i)
        res[i] = data[i] + v.data[i];

    return res;
}

Vector Vector::operator-(Vector const &v) const {
    if (size != v.size)
        throw -1;

    Vector res(size);
    for (size_t i = 0; i < size; ++i)
        res = data[i] - v.data[i];

    return res;
}

Vector Vector::operator-() const {
    Vector res(size);
    for (size_t i = 0; i < size; ++i)
        res = -data[i];

    return res;
}

void Vector::transpose() {
    metric = ((metric == col) ? row : col);
}

double Vector::scalar(Vector const &v) const {
    if (size != v.size)
        throw -1;

    double res = .0;
    for (size_t i = 0; i < size; ++i)
        res += data[i] * v.data[i];

    return res;
}

Vector &Vector::operator=(Vector const &v) {
    data = v.data;
    size = v.size;
    metric = v.metric;

    return *this;
}

void Vector::copyOnWrite() {
    std::cout << "Copy : Vector\n";

    SPtr<double> tmp = new double[size];

    for (int i = 0; i < size; ++i)
        tmp[i] = data[i];

    data = tmp;
}

Vector &Vector::operator+=(Vector const &v) {
    if (data.ref_count() > 1)
        this->copyOnWrite();

    for (size_t i = 0; i < size; ++i)
        data[i] += v.data[i];

    return *this;
}

Vector& Vector::operator-=(Vector const &v) {
    if (data.ref_count() > 1)
        this->copyOnWrite();

    for (size_t i = 0; i < size; ++i)
        data[i] -= v.data[i];

    return *this;
}

Vector &Vector::operator*=(double d) {
    if (data.ref_count() > 1)
        this->copyOnWrite();

    for (size_t i = 0; i < size; ++i)
        data[i] *= d;

    return *this;
}

Vector Vector::operator/(double d) const {
    Vector res(size);

    for (size_t i = 0; i < size; ++i)
        res.data[i] = data[i] / d;

    return res;
}

Vector &Vector::operator/=(double d) {
    if (data.ref_count() > 1)
        copyOnWrite();

    for (size_t i = 0; i < size; ++i)
        data[i] /= d;

    return *this;
}

size_t Vector::getSize() const {
    return size;
}

VectorSlice Vector::slice(size_t begin, size_t end, size_t step) const {
    if (step == 0)
        throw -1;

    VectorSlice res(this, begin, end, step);

    return res;
}

Vector::orientation Vector::getMetric() const {
    return metric;
}

Vector Vector::operator*(Matrix const &m) const {
    if (size != m.rowsCount())
        throw -1;

    Vector res(m.colsCount());
    for (size_t i = 0; i < res.size; i++)
        for (size_t j = 0; j < size; j++)
            res.data[i] += data[j] * m(j * m.rowsCount(), i);

    return res;
}

Matrix Vector::operator*(Vector const &v) const {
    if (metric == row && v.metric == col)
        return this->rowToCol(v);
    else if (metric == col && v.metric == row)
        return colToRow(v);
    else
        throw -1;
}

Matrix Vector::rowToCol(Vector const &v) const {
    if (size != v.size)
        throw -1;

    Matrix res(1, 1);
    for (size_t i = 0; i < size; i++)
        res.data[0] += data[i] * v.data[i];

    return res;
}

Matrix Vector::colToRow(Vector const &v) const {
    Matrix res(size, v.size);
    for (size_t i = 0; i < size; i++)
        for (size_t j = 0; j < v.size; j++)
            res(i, j) = data[i] * v.data[j];

    return res;
}

void Vector::setElem(size_t pos, double val) {
    if (data.ref_count() > 1)
        copyOnWrite();

    data[pos] = val;
}

double Vector::getElem(size_t pos) const {
    return data[pos];
}

Vector::Vector(VectorSlice const &vs) : size(vs.size), metric(vs.parent->metric) {
    data = new double[size];

    for (size_t i = 0; i < size; ++i)
        data[i] = vs[i];
}

Vector &Vector::operator=(VectorSlice const &vs) {
    size = vs.size;
    metric = vs.parent->metric;
    data = new double[size];

    for (size_t i = 0; i < size; ++i)
        data[i] = vs[i];
}

Vector::operator VectorSlice() const {
    return VectorSlice(this, 0, size - 1);
}

Vector Vector::operator+(VectorSlice const &vs) {
    return vs + *this;
}

Vector Vector::operator-(VectorSlice const &vs) {
    return (VectorSlice)*this - vs;
}

double Vector::scalar(VectorSlice const &vs) {
    return ((VectorSlice)(*this)).scalar(vs);
}

Vector operator*(double d, Vector const &v) {
    return v * d;
}

std::ostream &operator<<(std::ostream &os, const Vector &v) {
    for (int i = 0; i < v.getSize(); ++i)
        os << v[i] << ' ';

    return os;
}
