#ifndef INC_3SEM_LAB2_SPTR_HPP
#define INC_3SEM_LAB2_SPTR_HPP

#include <iostream>

template <typename T>
class RealSPtr {
public:
	explicit RealSPtr(T *data) : data_(data), ref_count_(1) {}

	~RealSPtr() {
		if (data_ != nullptr)
			delete[] data_;
	}

	size_t ref_count() const {
		return ref_count_;
	}

	T *data() const {
		return data_;
	}

	size_t operator++() {
		return ++ref_count_;
	}

	size_t operator--() {
		return --ref_count_;
	}

	T &operator*() const {
		return *data_;
	}

private:
	T *data_;
	size_t ref_count_;
};

template <typename T>
class SPtr {
public:
	SPtr(T *ptr = nullptr) : real_sptr(nullptr) {
		if (ptr != nullptr)
			real_sptr = new RealSPtr<T>(ptr);
	}

	~SPtr() {
		if (real_sptr != nullptr) {
			if (--(*real_sptr) == 0)
				delete real_sptr;
		}
	}

	SPtr(const SPtr &ptr) {
		if (ptr.get() != nullptr) {
			++(*ptr.real_sptr);
		}
		real_sptr = ptr.real_sptr;
	}

	SPtr &operator=(const SPtr &ptr) {
		if (this != &ptr) {
			if (real_sptr != nullptr) {
				if (--(*real_sptr) == 0)
					delete real_sptr;
			}
			if (ptr.get() != nullptr) {
				++(*ptr.real_sptr);
			}
			real_sptr = ptr.real_sptr;
		}
		return *this;
	}

	T *get() const {
		if (real_sptr != nullptr)
			return real_sptr->data();
		return nullptr;
	}

	void reset(T *ptr = 0) {
		if (real_sptr != nullptr) {
			if (--(*real_sptr) == 0) {
				delete real_sptr;
			}
		}
		real_sptr = new RealSPtr<T>(ptr);
	}

    size_t ref_count() {
        return real_sptr->ref_count();
    }

    T operator[](size_t i) const {
        return real_sptr->data()[i];
    }

    T &operator[](size_t i) {
        return real_sptr->data()[i];
    }

	T &operator*() const {
		return **real_sptr;
	}

	T *operator->() const {
		return real_sptr->data();
	}
	
private:
	RealSPtr<T> *real_sptr;
};

#endif