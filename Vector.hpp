//
// Created by stephen on 3/4/17.
//

#ifndef INC_3SEM_LAB2_VECTOR_HPP
#define INC_3SEM_LAB2_VECTOR_HPP


#include "SPtr.hpp"
#include "Matrix.hpp"
#include "ElemProxy.hpp"
#include "VectorSlice.hpp"

class Matrix;
class VectorSlice;

class Vector {
public:
    enum orientation {
        row,
        col
    };
private:
    SPtr<double> data;
    orientation metric;
    size_t size;

    void copyOnWrite();
    Matrix rowToCol(Vector const &v) const;
    Matrix colToRow(Vector const &v) const;
    void setElem(size_t pos, double val);
    double getElem(size_t pos) const;

public:
    Vector(size_t n, orientation m = row);
    Vector(VectorSlice const &vs);

    Vector &operator=(Vector const &v);
    Vector &operator=(VectorSlice const &vs);
    double operator[](size_t i) const;   //обращение к элементу
    ElemProxy<Vector> operator[](size_t i);        //обращение к элементу

    Vector operator-() const;
    Vector operator-(Vector const &v) const;
    Vector operator+(Vector const &v) const;
    Vector operator*(double d) const;
    Vector operator/(double d) const;

    Vector &operator+=(Vector const &v);
    Vector &operator-=(Vector const &v);
    Vector &operator*=(double d);
    Vector &operator/=(double d);

    Matrix operator*(Vector const &v) const;
    Vector operator*(Matrix const &m) const;

    operator VectorSlice() const;

    double scalar(Vector const &v) const;
    void transpose();
    size_t getSize() const;
    orientation getMetric() const;

    VectorSlice slice(size_t begin, size_t end, size_t step = 1) const;
    Vector operator+(VectorSlice const &vs);
    Vector operator-(VectorSlice const &vs);
    double scalar(VectorSlice const &vs);

    friend class ElemProxy<Vector>;
    friend class VectorSlice;
    friend class Matrix;
};

Vector operator*(double d, Vector const &v);
std::ostream &operator<<(std::ostream &os, const Vector &v);

#endif //INC_3SEM_LAB2_VECTOR_HPP
